terraform {
  required_providers {
    vkcs = {
      source  = "vk-cs/vkcs"
      version = "~> 0.5.2"
    }
  }
}

provider "vkcs" {
  # Your user account.
  username = "crazyflash@mail.ru"

  # The password of the account
  password = "Aa5141048"

  # The tenant token can be taken from the project Settings tab - > API keys.
  # Project ID will be our token.
  project_id = "3263ed6506704d71bd114d88e5d11843"

  # Region name
  region = "RegionOne"

  auth_url = "https://infra.mail.ru:35357/v3/"
}

variable "image_flavor" {
  type    = string
  default = "Ubuntu-22.04-202208"
}

variable "compute_flavor" {
  type    = string
  default = "STD2-1-2"
}

variable "key_pair_name" {
  type    = string
  default = "local-win"
}

variable "availability_zone_name" {
  type    = string
  default = "GZ1"
}

#NETWORK

data "vkcs_networking_network" "extnet" {
  name = "internet"
}

resource "vkcs_networking_network" "network" {
   name = "net"
}

resource "vkcs_networking_subnet" "subnetwork" {
   name       = "subnet_1"
   network_id = vkcs_networking_network.network.id
   cidr       = "192.168.199.0/24"
}

resource "vkcs_networking_router" "router" {
   name                = "router"
   admin_state_up      = true
   external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "db" {
   router_id = vkcs_networking_router.router.id
   subnet_id = vkcs_networking_subnet.subnetwork.id
}

#MAIN.TF

data "vkcs_compute_flavor" "compute" {
  name = var.compute_flavor
}

data "vkcs_images_image" "compute"{
  name = var.image_flavor
}

resource "vkcs_compute_instance" "nginx_1" {
  name              = "nginx_1"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  key_pair          = var.key_pair_name
  security_groups   = ["my_group"]
  availability_zone = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-hdd"
    volume_size           = 18
    boot_index            = 0
    delete_on_termination = true
  }  

  config_drive = true

  network {
    uuid = vkcs_networking_network.network.id
    fixed_ip_v4 = "192.168.199.110"
  }

  user_data = "${file("${path.module}/user_data.sh")}"
}

resource "vkcs_compute_instance" "nginx_2" {
  name              = "nginx_2"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  key_pair          = var.key_pair_name
  security_groups   = ["my_group"]
  availability_zone = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-hdd"
    volume_size           = 18
    boot_index            = 0
    delete_on_termination = true
  }

  config_drive = true

  network {
    uuid = vkcs_networking_network.network.id
    fixed_ip_v4 = "192.168.199.111"
  }

  user_data = "${file("${path.module}/user_data.sh")}"
}

  #user_data = "${file("${path.module}/user_data.sh")}"

  #depends_on = [
    #vkcs_networking_network.network
  #]
##### Balancer
resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"
  vip_subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
  tags = ["tag1"]
}

resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 8080
  loadbalancer_id = "${vkcs_lb_loadbalancer.loadbalancer.id}"
}

resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP"
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}

resource "vkcs_lb_member" "member_1" {
  address = "192.168.199.110"
  protocol_port = 8080
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
  weight = 0
}

resource "vkcs_lb_member" "member_2" {
  address = "192.168.199.111"
  protocol_port = 8080
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
}

#####
resource "vkcs_networking_floatingip" "fip" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip" {
  floating_ip = vkcs_networking_floatingip.fip.address
  instance_id = vkcs_compute_instance.nginx_1.id
}

output "instance_fip" {
  value = vkcs_networking_floatingip.fip.address
}

resource "vkcs_networking_floatingip" "fip_2" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip_2" {
  floating_ip = vkcs_networking_floatingip.fip_2.address
  instance_id = vkcs_compute_instance.nginx_2.id
}

output "instance_fip_2" {
  value = vkcs_networking_floatingip.fip_2.address
}

data "vkcs_lb_loadbalancer" "loadbalancer" {
  id = vkcs_lb_loadbalancer.loadbalancer.id
  # This is unnecessary in real life.
  # This is required here to let the example work with loadbalancer resource example. 
  depends_on = [vkcs_lb_loadbalancer.loadbalancer]
}

data "vkcs_networking_port" "app-port" {
  port_id = data.vkcs_lb_loadbalancer.loadbalancer.vip_port_id
}

output "used_vips" {
  value = data.vkcs_networking_port.app-port.all_fixed_ips
  description = "IP addresses of the app"
}

